<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-optionality-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Optionality;

use ReflectionException;

/**
 * Optionality class file.
 * 
 * This class represents optionalities for a relational model.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.StaticAccess")
 */
enum Optionality : string implements OptionalityInterface
{
	
	/**
	 * Finds the optionality with the null and empty allowed values.
	 * 
	 * @param boolean $isNullAllowed
	 * @param boolean $isEmptyAllowed
	 * @return OptionalityInterface
	 * @throws ReflectionException
	 */
	public static function findBy(bool $isNullAllowed, bool $isEmptyAllowed) : OptionalityInterface
	{
		foreach(Optionality::cases() as $optionality)
		{
			/** @var Optionality $optionality */
			if($optionality->isEmptyAllowed() === $isEmptyAllowed && $optionality->isNullAllowed() === $isNullAllowed)
			{
				return $optionality;
			}
		}
		
		return Optionality::NULL_ALLOWED_EMPTY_ALLOWED;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Optionality\OptionalityInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $this === $object;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Optionality\OptionalityInterface::isNullAllowed()
	 */
	public function isNullAllowed() : bool
	{
		return \strpos($this->value, 'yn') !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Optionality\OptionalityInterface::isEmptyAllowed()
	 */
	public function isEmptyAllowed() : bool
	{
		return \strpos($this->value, 'ye') !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Optionality\OptionalityInterface::mergeWith()
	 * @throws ReflectionException
	 */
	public function mergeWith(OptionalityInterface $other) : OptionalityInterface
	{
		return static::findBy(
			$this->isNullAllowed() || $other->isNullAllowed(),
			$this->isEmptyAllowed() || $other->isEmptyAllowed(),
		);
	}

	case NO_NULL_NO_EMPTY = 'nn-ne';
	case NO_NULL_EMPTY_ALLOWED = 'nn-ye';
	case NULL_ALLOWED_NO_EMPTY = 'yn-ne';
	case NULL_ALLOWED_EMPTY_ALLOWED = 'yn-ye';
	
}
