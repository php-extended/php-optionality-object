<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-optionality-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Optionality\Optionality;
use PHPUnit\Framework\TestCase;

/**
 * OptionalityTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Optionality\Optionality
 *
 * @internal
 *
 * @small
 */
class OptionalityTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Optionality
	 */
	protected Optionality $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('nn-ne', $this->_object->value);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = Optionality::NO_NULL_NO_EMPTY;
	}
	
}
